# Link Formatter query fix

This module fix query parameters duplication.
See `https://www.drupal.org/node/2885351` for more info.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/link_formatter_query_fix).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/link_formatter_query_fix).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Link core module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable module.
2. Rebuild cache.
3. Set 'Link (query duplication fix)' in display settings of your link field.


## Maintainers

- Ivan Abramenko - [levmyshkin](https://www.drupal.org/u/levmyshkin)
- Nikolay Shapovalov - [zniki.ru](https://www.drupal.org/u/znikiru)
